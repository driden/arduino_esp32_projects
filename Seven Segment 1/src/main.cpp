#include <Arduino.h>
#include <SevSeg.h>
#include <unistd.h>

/**
https://en.wikipedia.org/wiki/Seven-segment_display
*/
// byte digitPins[] = {12, 9, 8, 6};
// byte segmentPins[] = {11, 7, 4, 2, 1, 10, 5, 3};

byte digitPins[] = {10, 11, 12, 13};
byte segmentPins[] = {9, 2, 3, 5, 6, 8, 7, 4};

//
short DELAY = 1;
short CURRENT = 0;

void setVoltagesToSegments(byte voltages[])
{
  for (short i = 0; i < 7; i++)
  {
    digitalWrite(segmentPins[i], voltages[i]);
  }
}

// Segment Rendering
void zero()
{
  byte segmentVoltages[] = {HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, LOW};
  setVoltagesToSegments(segmentVoltages);
}

void one()
{
  byte segmentVoltages[] = {LOW, HIGH, HIGH, LOW, LOW, LOW, LOW};
  setVoltagesToSegments(segmentVoltages);
}

void two()
{
  byte segmentVoltages[] = {HIGH, HIGH, LOW, HIGH, HIGH, LOW, HIGH};
  setVoltagesToSegments(segmentVoltages);
}

void three()
{
  byte segmentVoltages[] = {HIGH, HIGH, HIGH, HIGH, LOW, LOW, HIGH};
  setVoltagesToSegments(segmentVoltages);
}

void four()
{
  byte segmentVoltages[] = {LOW, HIGH, HIGH, LOW, LOW, HIGH, HIGH};
  setVoltagesToSegments(segmentVoltages);
}

void five()
{
  byte segmentVoltages[] = {HIGH, LOW, HIGH, HIGH, LOW, HIGH, HIGH};
  setVoltagesToSegments(segmentVoltages);
}

void six()
{
  byte segmentVoltages[] = {HIGH, LOW, HIGH, HIGH, HIGH, HIGH, HIGH};
  setVoltagesToSegments(segmentVoltages);
}

void seven()
{
  byte segmentVoltages[] = {HIGH, HIGH, HIGH, LOW, LOW, LOW, LOW};
  setVoltagesToSegments(segmentVoltages);
}

void eight()
{
  byte segmentVoltages[] = {HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH};
  setVoltagesToSegments(segmentVoltages);
}

void nine()
{
  byte segmentVoltages[] = {HIGH, HIGH, HIGH, HIGH, LOW, HIGH, HIGH};
  setVoltagesToSegments(segmentVoltages);
}

void digit(short digitNumber)
{
  for (short i = 0; i < 4; i++)
  {
    if (i == digitNumber - 1)
    {
      digitalWrite(digitPins[i], LOW);
    }
    else
    {
      digitalWrite(digitPins[i], HIGH);
    }
  }
}

void setDelay(short milliseconds)
{
  DELAY = milliseconds;
}

void setTime(short startSeconds)
{
  if (startSeconds <= 0)
  {
    printf("The time should be more then 0");
  }
  if (startSeconds > 9999)
  {
    printf("This device does not accept more than 9999 seconds");
  }
  CURRENT = startSeconds;
}

char *getCurrentTimeStr()
{
  char strDigits[4];
  sprintf(strDigits, "%d", CURRENT);
  short zeroBy = 0;
  for (short i = 0; i < 4; i++)
  {
    if (strDigits[3 - i] != NULL)
    {
      zeroBy = i;
      break;
    }
  }
  static char processedDigits[4];
  for (short i = 0; i < 4; i++)
  {
    if (i < zeroBy)
    {
      processedDigits[i] = '0';
    }
    else
    {
      processedDigits[i] = strDigits[i - zeroBy];
    }
  }

  return processedDigits;
}

void setSegmentTo(char numStr)
{
  switch (numStr)
  {
  case '0':
    zero();
    break;
  case '1':
    one();
    break;
  case '2':
    two();
    break;
  case '3':
    three();
    break;
  case '4':
    four();
    break;
  case '5':
    five();
    break;
  case '6':
    six();
    break;
  case '7':
    seven();
    break;
  case '8':
    eight();
    break;
  case '9':
    nine();
    break;
  }
}

void display(char *digits)
{
  for (short i = 0; i < 4; i++)
  {
    digit(i + 1);
    setSegmentTo(digits[i]);
    delay(DELAY);
  }
}

void setup()
{
  for (short i = 0; i < 4; i++)
  {
    pinMode(digitPins[i], OUTPUT);
  }
  for (short i = 0; i < 7; i++)
  {
    pinMode(segmentPins[i], OUTPUT);
  }
  setTime(10);
}

void loop()
{
  for (int i = 9999; i >= 0; i--)
  {
    setTime(i);
    short remainedInternval = 250;
    while (remainedInternval > 0)
    {
      display(getCurrentTimeStr());
      remainedInternval--;
    }
  }
}
